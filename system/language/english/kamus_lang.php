<?php

// MENU
$lang["tentang"] 	= "about";
$lang["fitur"]	 	= "feature";
$lang["tim"] 		= "team";
$lang["kontak"] 	= "contact";
$lang["blog"] 		= "blog";
$lang["download"] 	= "download";
$lang["pesan"] 		= "order";
$lang["masuk"] 		= "login";
$lang["bhs_indo"] 	= "Indonesian";
$lang["bhs_ing"] 	= "English";
$lang["daftar"]		= "SIGN UP";
$lang["keluar"]		= "Logout";
$lang["pass"]		= "PASSWORD";
$lang["error_msg"]	= "The email and password you entered don't match.";


$lang["homepage"]  = "home";
$lang["nama_leng"]  = "Full Name";
$lang["batal"]		= "cancel";

$lang["login_dulu"]  = "login first";

// TENTANG CAKRA
$lang["ttg_cakra"] 		= "ABOUT CAKRA";
$lang["des_ttg_cakra"] 	= "Cakra is a therapy application for autistic, ADD, ADHD, Asperger, Down Syndrome, mental retardation and slow learner using interactive technology. With Cakra, parents can perform therapy at home. With attractive appearance and easy to understand, therapy process will be much easier and fun. Developed together with autism expert, features in Cakra application have been validated. Equipped with evaluation and report so parents can see their child's therapy development easily.";
$lang["intensif"] 		= "INTENSIVE";
$lang["dekat"] 			= "CLOSE";
$lang["teknologi"] 		= "TECHNOLOGY";
$lang["des_intensif"] 	= "CAKRA application helps you to intesify therapy at home to enhance the development of child's ability";
$lang["des_dekat"] 		= "CAKRA application can create home as the nearest therapy place so it can train child's adaptation and socialization";
$lang["des_teknologi"] 	= "Using technology as a stimulus to accelerate motor, speaking and academic of children with special needs.";


// FITUR
$lang["evaluasi"] 		= "EVALUATION";
$lang["eval_1"] 		= "based on ATEC (Autism Treatment Evaluation Checklist) standard";
$lang["eval_2"] 		= "developed by Autism Research Institute";
$lang["eval_3"] 		= "show the percentage of child's aspect";
$lang["eval_4"] 		= "show the child's development every month";
$lang["terapi"] 		= "THERAPY";
$lang["terapi_1"] 		= "CONSISTS OF TWO MODELS: FREE MODE AND STRUCTURED";
$lang["terapi_2"] 		= "complete therapy for autism (basic stage, intermediate, and advanced)";
$lang["terapi_3"] 		= "using ABA method issued by LOVAAS";
$lang["terapi_4"] 		= "consist of 3 levels, 22 categories, 122 therapy stages and THOUSANDS content";
$lang["laporan"] 		= "REPORT";
$lang["lap_1"] 			= "Complete report: recapitulation from all aspects of the report";
$lang["lap_2"] 			= "Monthly report: monthly recapitulation of all aspect";
$lang["lap_3"] 			= "Monthly report: monthly recapitulation of achievement progress of every therapy material";

// EDISI CAKRA
$lang["edisi_cakra"] 	= "Cakra Edition";
$lang["tahapan_terapi"] = "Therapy Stages";
$lang["thp_dasar"] 		= "Basica";
$lang["thp_menengah"] 	= "Intermediate";
$lang["thp_lanjut"] 	= "Advanced";
$lang["fasilitas"] 		= "Facilities";
$lang["fas_terapi_int"] = "Structured Therapy";
$lang["fas_mentoring"] 	= "Monthly Monitoring";
$lang["fas_laporan"] 	= "Development Report";
$lang["fas_pemakaian"] 	= "Usage";
$lang["konten"] 		= "Contents";
$lang["kon_resource"] 	= "RESOURCE";
$lang["kon_sound_video"]= "SOUND / VIDEO REINFORCE";
$lang["kon_info"] 		= "INFORMATION";
$lang["kon_vid_tutor"] 	= "VIDEO TUTORIAL";
$lang["jenis"] 			= "types";
$lang["tersedia"] 		= "available";
$lang["selamanya"]		= "Lifetime";
$lang["buah"] 			= "pieces";
$lang["bisa_berubah"] 	= "can change";
$lang["tahap"] 			= "stages";
$lang["tetap"]			= "fixed";

// PENGHARGAAN
$lang["penghargaan"] 		= "Awards";
$lang["peng_imgcup"]		= "Winner of World Citizenship Category Indonesia";
$lang["peng_imgcup2"]		= "SEMIFINALIST of World Citizenship Category  International";
$lang["despeng_imgcup"] 	= "Imagine cup is the biggest competition organized by Microsoft Corp.";

$lang["peng_indigo"] 		= "Winner startup of Health application category";
$lang["despeng_indigo"] 	= "Indigo incubator is an incubation startup owned by Telkom Indonesia";

$lang["peng_lcen"] 			= "Winner of Medical Electronics & Assistive Technology category";
$lang["despeng_lcen"] 		= "LCEN XVII is the 17th National Electrotechnical Idea Competition organized by ITS.";

$lang["peng_gem_ipl"] 		= "Winner of Software Innovation Category";
$lang["despeng_gem_ipl"] 	= "GEMASTIK 6 is  the 6th National Student Performance in Information Technology organized by Directorate of Higher Education (DIKTI)";

$lang["peng_gem_rpl"] 		= "Winner of Software Engineering Category";
$lang["despeng_gem_rpl"] 	= "GEMASTIK 6 is  the 6th National Student Performance in Information Technology organized by Directorate of Higher Education (DIKTI)";

$lang["peng_itsexpo"] 		= "Winner of Application Development Category";
$lang["despeng_itsexpo"] 	= "IT Contest is one of a series of national competitions in ITS Expo";

$lang["peng_inaicta"] 		= "SPECIAL Mention of Tertiary Student Project Category";
$lang["despeng_inaicta"] 	= "INAICTA is Indonesia ICT Award organized by Ministry of Communication and Informatics.";

$lang["peng_snitch"] 		= "WINNER of Application Development Category";
$lang["despeng_snitch"] 	= "SNITCH is a competition in Informatics field organized by ITS.";

$lang["peng_pimnas"] 		= "Winner of Software Engineering Category";
$lang["despeng_pimnas"] 	= "PIMNAS is the National Student Sciencitic Week organized by Directorate of Higher Education.";

$lang["peng_mandiri"] 		= "Winner of Digital Category";
$lang["despeng_mandiri"] 	= "Mandiri Young Technopreneur is a competition for young entrepreneurs who have technology-based products.";

$lang["peng_nomi"] 			= "NOMINATOR Touth Zitizen Entrepreneurship Competition";
$lang["despeng_nominator"] 	= "International contest organized by The Goi Peace Foundation, Stiftung Entrepreneurship (Berlin) and UNESCO.";


// KATA MEREKA
$lang["kata_mereka"] = "What They Said";

$lang["kata_illy"] 	= "Cakra is the compelete application and suitable for autistic.";
$lang["illy"] 		= "HJ. ILLY YUDIONO";
$lang["jab_illy"] 	= "Autism expert and the owner of CAKRA Autism Center, Surabaya";

$lang["kata_fitri"] = "A very good application. We have never used an application like this before.";
$lang["fitri"] 		= "Fitriani";
$lang["jab_fitri"] 	= "Teacher in a therapy place";

$lang["kata_yudi"] 	= "CAKRA can compete with any other application which has long been on the market.";
$lang["yudi"] 		= "H. Yudiono";
$lang["jab_yudi"] 	= "owner of Autism Foundation ";

$lang["kata_fatir"] = "Good.";
$lang["fatir"] 		= "Fatir";
$lang["jab_fatir"] 	= "Autistic";

$lang["kata_lolly"] = "Cakra is a very potential application.";
$lang["lolly"] 		= "Ir. Lolly Amalia Abdullah, M.Sc.";
$lang["jab_lolly"] 	= "Heads of R & D center of informatics applications and Information and Public Communication Ministry of Tourism and Creative Economy";

$lang["kata_dian"] 	= "CAKRA, the most complete austism therapy application I have ever seen. And it is very useful..";
$lang["dian"] 		= "Andreas Diantoro";
$lang["jab_dian"] 	= "President Director of Microsoft Indonesia";



$lang["tim_kami"] 			= "our team";
$lang["tombol_download"] 	= "DOWNLOAD NOW";
$lang["tombol_pesan"] 		= "ORDER NOW";


// KONTAK
$lang["kediaman"] 	= "OUR RESIDENCE";
$lang["alamat"] 	= "ADDRESS";
$lang["telp"] 		= "TELEPHONE";
$lang["email"] 		= "EMAIL";
$lang["jam_kerja"] 	= "OFFICE HOURS";
$lang["hari_1"] 	= "MONDAY - FRIDAY";
$lang["jam_1"] 		= "09.00 - 18.00";
$lang["hari_2"] 	= "SATURDAY - SUNDAY";
$lang["jam_2"] 		= "CLOSED";
$lang["masukan"] 	= "GIVE US SUGGESTION";
$lang["nama"] 		= "NAME";
$lang["komentar"] 	= "COMMENT";
$lang["kirim"] 		= "SEND";


// FOOTER
$lang["disponsori"] 	= "Sponsored by :";
$lang["dibuat"] 		= "Made by :";