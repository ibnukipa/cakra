<?php

function cakra_resources(){
	wp_enqueue_style('style3', get_template_directory_uri() . '/css/bootstrap.css');
	wp_enqueue_style('style4', get_template_directory_uri() . '/css/animate.css');
	wp_enqueue_style('style2', get_template_directory_uri() . '/css/style.css');
	wp_enqueue_style('style', get_template_directory_uri() . '/style.css');
	wp_enqueue_script('script1', get_template_directory_uri() . '/js/jquery.min.js');
	wp_enqueue_script('script2', get_template_directory_uri() . '/js/wow.min.js');
	wp_enqueue_script('script3', get_template_directory_uri() . '/js/move-top.min.js');
	wp_enqueue_script('script4', get_template_directory_uri() . '/js/easing.js');
	wp_enqueue_script('script5', get_template_directory_uri() . '/js/nav.js');
	wp_enqueue_script('script6', get_template_directory_uri() . '/js/wow.js');
	wp_enqueue_script('script7', get_template_directory_uri() . '/js/easing2.js');
	wp_enqueue_script('script8', get_template_directory_uri() . '/js/customTheme.js');
				
}
add_action('wp_enqueue_scripts', 'cakra_resources');

function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

//navigation menus
register_nav_menus(array(
	'primary' =>  __( 'Primary Menu'),
	'footer' => __( 'Footer Menu'),
));

/*  Add responsive container to embeds*/ 
function activate_jquery() {
	wp_enqueue_script( 'jquery');
}
add_action( 'wp_enqueue_scripts', 'activate_jquery' );

function et_excerpt_length($length) {
    return 20;
}
add_filter('excerpt_length', 'et_excerpt_length');
 
/* Add a link  to the end of our excerpt contained in a div for styling purposes and to break to a new line on the page.*/
 
function et_excerpt_more($more) {
    global $post;
    return '<div class="view-full-post"><a href="'. get_permalink($post->ID) . '" class="view-full-post-btn">See More</a></div>';
}
add_filter('excerpt_more', 'et_excerpt_more');
add_theme_support( 'post-thumbnails' );

/*pagination*/
function pagination_number($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='halaman'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>";
     }
}

//Enqueue the Dashicons script
add_action( 'wp_enqueue_scripts', 'amethyst_enqueue_dashicons' );
function amethyst_enqueue_dashicons() {
	wp_enqueue_style( 'amethyst-dashicons-style', get_stylesheet_directory_uri(), array('dashicons'), '1.0' );
}

?>
