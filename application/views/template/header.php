<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="CAKRA - Because everyone is special">
    <meta property="og:url" content="http://cakra-app.com">
    <meta property="og:description" content="Autism Therapy Aplication">
    <meta property="og:image" content="<?php echo base_url(); ?>assets/img/meta.png">
    <meta property="og:type" content="website">

    <title>CAKRA | Aplikasi Terapi Autisme yang Praktis, Mudah, dan Menyenangkan</title>
    <meta name="description" content="Aplikasi CAKRA untuk Terapi Autisme yang praktis, mudah, dan menyenangkan. Kini Anda bisa melakukan terapi mandiri di rumah"/>
    <meta name="keywords" content="cakra, autism, autisme, autis, autistic, autist, anak, keluarga, desktop pc, windows, ios, terapi, psikologi, special needs, asperger, adhd, hiperaktif"/>

    <link rel="icon" href="<?=base_url()?>assets/img/favicon.png" type="image/png">
	
	<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/reset.css" />
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
    <div id="preloader"></div>