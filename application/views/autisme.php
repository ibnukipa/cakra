<section id="autisme">
    <div class="container">
        <h3>Apa itu Autisme?</h3>
        <div class="cerita-opening">
            <div class="gambar">
                <img src="<?php echo base_url(); ?>assets/img/testimoni/illy.jpg">
            </div>
            <p>
                <i>
                    "Autisme adalah gangguan perkembangan pada anak bisa diketahui dan harus diketahui 
                    sebelum anak berusia 3 tahun. Ada 3 hal yang mencolok dari sekian banyak tanda autism 
                    yakni gangguan komunikasi verbal maupun non verbal,  gangguan sosialisasi, interaksi dan 
                    adaptasi dan adanya perilaku aneh. Komunikasi verbal contohnya anak bisa bernyanyi 
                    seperti Indonesia raya namun ketika ditanya siapa namamu tidak tau karena anak hanya membeo.  
                    Jika non verbal tidak ada kalimat apapun yang keluar dan bahasa isyarat juga tidak berkembang 
                    misal ingin sesuatu tidak bisa menunjuk"
                </i>
                 - <strong>Illy Yudiono</strong> pemilik dan pendiri cakra autism center.

            </p>

            <p>Autism itu disebut autism spectrum disorder. Disebut spectrum karena gejalanya sangat banyak. Setiap anak berbeda kecuali 3 hal dasar diatas.</p>
            <p>Sebenarnya spectrum autisme tidak bisa dibilang cacat karena tidak ada permasalahan di ototnya. Jadi tidak diperlukan latihan fisik tapi yang diperlukan adalah asupan di otaknya. Yakni pembelajaran kognitifnya.</p>
            <p>Sejak awal tahun 1990 metode ABA untuk terapi di rekomendasikan sebagai metode yang paling efektif dalam menangani spectrum autism, sehingga saat ini terjadi peningkatan untuk intervensi autism, namun tidak diiringi dengan ketersediaan klinik terapi. Namun pada tahun 2001 beberapa orang tua dari inggris mencoba melakukan intervensi dengan model homebase dan orang tua sebagai program manager. Intervesi tersebut dievaluasi setelah 1 tahun dan dilaporkan tidak menunjukan peningkatan IQ yang siqnifikan pada anak spectrum autism seperti yang dilakukan oleh UCLA Multi-site Young Autism Project yang diprakarsai oleh Prof. Lovaas setelah dilakukan analisa, didapatkan data intervensi yang melibatkan orang tua tersebut tidak menggunakan protocol yang sesuai dan intervensi lebih dari 3.5 tahun, dan rata-rata hanya 32 jam perminggu dan tidak ada supervise yang intensif. namun ketika dilakukan penelitian ulang pada tahun 2009 dimana anak mendapat intervensi sebelum berusia 4 tahun dan mendapat terapi 40 jam perminggu dan mendapat supervisi dari ahli secara rutin dan intensif. keberhasilan dari home based program dibanding di klinik terapis lebih baik.</p>
            <p>Untuk membantu terapi homebased dan yang sulit sekali menjangkau tempat terapi kami memperkenalkan </p>
            <div class="gambar" style="padding: 30px 0">
                <img class="logo-cakra" src="<?php echo base_url(); ?>assets/img/logo.svg">
            </div>
            <p>
                Dengan mengusung tagline penuh optimisme 
                <i style="color: #1ACDB0">“Because everyone is special”</i>
                CAKRA meyakini bahwa autistic person punya hak yang sama dalam kehidupan. 
                Begitu juga hak dalam menggapai sukses. CAKRA punya mimpi untuk menyebarkan harapan dan semangat optimisme 
                kepada seluruh anak pengidap autistic dimanapun di seluruh pelosok negeri tercinta ini.
            </p>
            <p style="text-align: center">
                <a href="<?php echo site_url('home/cakra') ?>" class="btn-flat" data-warna="cakra">Apa itu Cakra?</a> 
            </p>
        </div>
    </div>
</section>