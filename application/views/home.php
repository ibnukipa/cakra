<section id="opening">
    <div class="container opening">
        <div class="row" style="margin-bottom: 0">
            <div class="col s12">
                <h2 class="animated slideInDown">TEROBOSAN BARU SOLUSI TERAPI AUTISME PRAKTIS, MUDAH, DAN MENYENANGKAN.</h2>
                <p class="animated slideInDown">Setelah Anda membaca halaman ini, pandangan Anda terhadap autisme akan berubah drastis.</p>
            </div>
            <div class="col s12">
                <div class="col s12 m12 l6 animated slideInLeft">
                    <video style=" display: block; width: 100%" controls="">
                        <source src="<?php echo base_url()?>assets/video/cakra.mp4" type="video/mp4">
                    </video>
                </div>
                <div class="col s12 m12 l6 animated slideInRight" style="padding: 0;">
                    <div style="background-color: rgba(8, 64, 55, .08); margin: 0 0.75rem; height: 100%; border: 1px dashed rgba(6, 63, 55, .5)">
                        <div class="form-get-cakra">
                            <p style="margin-bottom: 5px;"><strong>CAKRA Bronze</strong></p>
                            <p style="padding: 0">Dapatkan hanya dengan mengisi form berikut:</p>
                        </div>
                        <div class="row">
                            <form class="col s12" method="POST" action="<?php echo site_url('home/get_bronze'); ?>">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <!-- <span>Nama</span> -->
                                        <input id="nama" name="nama" placeholder="Nama Lengkap" type="text" class="validate">
                                    </div>
                                    <div class="input-field col s12">
                                        <!-- <span>Nama</span> -->
                                        <input id="email" name="email" placeholder="Email" type="email" class="validate">
                                    </div>
                                    <div class="col s12" style="text-align: center">
                                        <input class="btn-flat" type="submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 center down animated infinite fadeInDown">
                <a href="#opening-content">
                    <i class="material-icons">expand_more</i>
                </a>
            </div>
        </div>
    </div>
</section>
<section id="opening-content">
    <div class="segitiga"></div>
    <div class="container">
        <div class="cerita-opening animated slideInRight">
            <a><i class="material-icons left">info</i>INFO PENTING</a>
            <br>
            <p>Pada 1887 seorang anak usia 8 tahun di <i>New Jersey</i>, Amerika Serikat, 
            diusir oleh gurunya karena dianggap terlalu lambat mengikuti pelajaran.
            Dan siapa sangka 20 tahun kemudian dia menjadi salah satu orang jenius yang pernah hidup di dunia. 
            Siapa Dia? Dialah <strong>Albert Einstein</strong>. Penemu teori Relativitas. Teori Energi Atom. Dan ratusan teori 
            fisika dan mekanika kuantum lainnya.</p>
        </div>
    </div>

    <div class="pernyataan">
        <h5>Kami akan coba awali dengan sebuah pernyataan:</h5>
        <p>Autisme bisa disembuhkan. <i style="font-style: italic">Autism is curable</i></p>
    </div>

    <div class="container">
        <div class="cerita-opening animated slideInRight">
            <p>Jika Anda berpikir bahwa anak dengan autisme sebagai beban dan tidak memiliki masa depan, maka secara tidak langsung Anda telah merampas hak mereka untuk sukses.</p>
            <p>Kemungkinan dari Anda yang membaca adalah orang tua dari anak penyandang autisme. Maka dengan segala kerendahan hati, kami bangga kepada Anda. </p>
            <p>Lalu, benarkah autisme bisa disembuhkan? Sebelum membahas pertanyaan tersebut, marilah kita lihat siapa sosok-sosok di bawah ini.</p>
            <div class="bingkai-foto-autis">
                <div class="row">
                    <div class="col s12">
                        <div class="carousel a">
                            <a class="carousel-item">
                                <img src="<?php echo site_url('assets/img/foto-autis/mark_twain.png'); ?>">
                                <p>Mark Twain</p>
                            </a>
                            <a class="carousel-item">
                                <img src="<?php echo site_url('assets/img/foto-autis/thomas_jefferson.png'); ?>">
                                <p>Thomas Jefferson</p>
                            </a>
                            <a class="carousel-item">
                                <img src="<?php echo site_url('assets/img/foto-autis/beethoven.png'); ?>">
                                <p>Beethoven</p>
                            </a>
                            <a class="carousel-item">
                                <img src="<?php echo site_url('assets/img/foto-autis/thomas_edison.png'); ?>">
                                <p>Thomas Edison</p>
                            </a>
                            <a class="carousel-item">
                                <img src="<?php echo site_url('assets/img/foto-autis/mozzart.png'); ?>">
                                <p>Mozart</p>
                            </a>
                            <a class="carousel-item">
                                <img src="<?php echo site_url('assets/img/foto-autis/albert.png'); ?>">
                                <p>Albert Einstein</p>
                            </a>
                            <a class="carousel-item">
                                <img src="<?php echo site_url('assets/img/foto-autis/richard_strauss.png'); ?>">
                                <p>Richard Strauss</p>
                            </a>
                            <a class="carousel-item">
                                <img src="<?php echo site_url('assets/img/foto-autis/vangogh.png'); ?>">
                                <p>Vangogh</p>
                            </a>
                            <a class="carousel-item">
                                <img src="<?php echo site_url('assets/img/foto-autis/emily.png'); ?>">
                                <p>Emily Dickinson</p>
                            </a>
                            <a class="carousel-item">
                                <img src="<?php echo site_url('assets/img/foto-autis/newton.png'); ?>">
                                <p>Isaac Newton</p>
                            </a>
                            <a class="carousel-item">
                                <img src="<?php echo site_url('assets/img/foto-autis/snow.png'); ?>">
                                <p>George Bernard Snow</p>
                            </a>
                            <a class="carousel-item">
                                <img src="<?php echo site_url('assets/img/foto-autis/ford.png'); ?>">
                                <p>Henry Ford</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <p>Pasti Anda sudah tidak asing dengan wajah-wajah tersebut. Mereka adalah manusia-manusia yang telah menorehkan karya emas di atas lembaran sejarah. Tapi tahukah Anda kalau mereka semua adalah penyandang autisme? </p>
            <p>Simak video berikut juga :</p>
            <div class="bingkai-foto-autis">
                <div class="row">
                    <div class="col s12">
                        <div class="col s12 m6 l6" style="padding: 0 5px 5px 0;">
                            <iframe height="234px" width="100%" src="https://www.youtube.com/embed/bsJbApZ5GF0" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <div class="col s12 m6 l6" style="padding: 0 0 5px 5px;">
                            <iframe height="234px" width="100%" src="https://www.youtube.com/embed/cu1ydyvfXtA" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <div class="col s12 m6 l6" style="padding: 5px 5px 0 0;">
                            <iframe height="234px" width="100%" src="https://www.youtube.com/embed/vqPN_-1I7pI" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <div class="col s12 m6 l6" style="padding: 5px 0 0 5px;">
                            <iframe height="234px" width="100%" src="https://www.youtube.com/embed/ox39ReQL3Xw" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <p style="text-align: center">
                Sebenarnya apa sih <strong>pengertian autisme</strong> itu? Baca artikel berikut: <br>
                <a href="<?php echo site_url('home/autisme'); ?>" class="btn-flat" data-warna="cakra">Apa itu Autisme?</a> 
            </p>
        </div>
    </div>
</section>