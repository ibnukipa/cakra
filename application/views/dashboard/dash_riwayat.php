<div class="bg-cakra">
    <div class="nama-content">
        <i class="material-icons left">history</i>
        <span>Riwayat Pemesanan</span>
    </div>
    <div class="isi-content">
        <div class="riwayat">
            <table class="striped">
                <thead>
                    <tr>
                        <th data-field="id">ID</th>
                        <th data-field="name">Produk</th>
                        <th data-field="tanggal">Tanggal Pesan</th>
                        <th data-field="status">Status</th>
                        <th class="center" data-field="action">Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php if(count($riwayat) == 0) { ?>

                    <?php } ?>
                    <?php for($i=0; $i<count($riwayat); $i++) { ?>
                        <tr>
                            <td>#<?php echo $riwayat[$i]->id; ?></td>
                            <td style="text-transform: capitalize">cakra <?php echo $riwayat[$i]->edition; ?></td>
                            <td><?php echo $riwayat[$i]->created; ?></td>
                            <td>
                                <div class="action">
                                    <a class="btn-flat" data-warna="<?php echo $riwayat[$i]->status; ?>">
                                        <?php echo $riwayat[$i]->status; ?>
                                    </a> 
                                </div>
                            </td>
                            
                            <td class="center">
                                <?php if($riwayat[$i]->status == 'pending') { ?>
                                <div class="action">
                                    <a href="#satu" class="modal-trigger btn-flat" data-warna="deep-blue" >
                                        <i class="material-icons left">done_all</i>
                                        konfirmasi
                                    </a>
                                    
                                    <a class="btn-flat" data-warna="red">
                                        <i class="material-icons left">close</i>
                                        batal
                                    </a> 
                                </div>
                                <?php } else { ?>
                                    -
                                <?php } ?>    
                            </td>
                            
                        </tr>
                    <?php } ?>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->load->view('template/pop_persetujuan'); ?>