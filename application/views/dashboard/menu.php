<div class="navbar-fixed dashboard-menu" style="z-index: 996">
    <nav id="menu-top" >
        <div class="nav-wrapper">
            <ul>
                <li class="menu-top-icon" >
                    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin: 0; margin-right: 10px">
                        <i class="material-icons left">menu</i>
                    </a>
                </li>
                <li class="menu-top-logo">
                    <a href="<?php echo base_url(); ?>">
                        <img class="disable_select" src="<?php echo base_url(); ?>assets/img/logo.svg">
                    </a>
                </li>
                <div class="right">
                    <li>
                        <a class="dropdown-button" href="#" data-activates='drop-tentang' style="font-weight: 600; text-transform: none">
                            <i class="material-icons left">account_circle</i>
                            <span class="hide-on-small-only"><?php echo $_SESSION['username']; ?></span>
                            <i class="material-icons inline-arrow-down deg">keyboard_arrow_up</i>
                        </a>
                        <ul id='drop-tentang' class='dropdown-content bayangan_2dp'>
                            <!-- <li>
                                <a href="<?php echo site_url('dashboard/profile'); ?>">
                                    <i class="material-icons left">face</i>
                                    <span class="hide-on-small-only">Profil</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('dashboard/pengaturan'); ?>">
                                    <i class="material-icons left">settings</i>
                                    <span class="hide-on-small-only">Pengaturan</span>
                                </a>
                            </li> -->
                            <li>
                                <a href="<?php echo site_url('dashboard/logout'); ?>">
                                    <i class="material-icons left">exit_to_app</i>
                                    <span class="hide-on-small-only">Logout</span>
                                </a>
                            </li>
                            <!-- <li><a href="#!">Testimonial</a></li> -->
                        </ul>
                    </li>

                    <!-- action menu -->
                    <!-- <li class="hide-on-small-only action daftar">
                        <a>
                            DAFTAR
                        </a>
                    </li>
                    <li class="hide-on-small-only action login ">
                        <a href="<?php echo site_url('user'); ?>">
                            LOGIN
                        </a>
                    </li> -->


                    
                </div>

            </ul>
            
        </div>
    </nav>
</div>

<ul class="side-nav fixed dashboard-nav" id="mobile-demo">
    <li>
        <a href="<?php echo base_url(); ?>">
            <img class="disable_select" src="<?php echo base_url(); ?>assets/img/logo.svg">
        </a>
    </li>
    <li class="<?php if($menu_aktif == 'dash') echo 'active-true'; ?>">
        <a href="<?php echo site_url('dashboard'); ?>">
            <i class="material-icons left">account_balance_wallet</i>
            <span>Dasboard</span>
        </a>
    </li>
    <li class="<?php if($menu_aktif == 'riwayat') echo 'active-true'; ?>">
        <a href="<?php echo site_url('dashboard/riwayat'); ?>">
            <i class="material-icons left">access_time</i>
            <span>Riwayat Pemesanan</span>
        </a>
    </li>
    <!-- <li class="<?php if($menu_aktif == 'cara_order') echo 'active-true'; ?>">
        <a href="<?php echo site_url('dashboard/cara_order'); ?>">
            <i class="material-icons left">speaker_notes</i>
            <span>Cara Order</span>
        </a>
    </li> -->
    <!-- <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                <a class="collapsible-header">
                    <i class="material-icons right">expand_more</i>
                    <span>Tentang Cakra</span>
                </a>
                <div class="collapsible-body">
                    <ul>
                        <li>
                            <a href="<?php echo site_url('home/autisme'); ?>">
                                Apa itu Autisme?
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('home/cakra'); ?>">
                                Apa itu Cakra?
                            </a>
                        </li>
                        <li>
                            <a href="#!">
                                Penghargaan Cakra
                            </a>
                        </li>
                        <li>
                            <a href="#!">
                                Testimonial
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </li> -->
    
</ul>