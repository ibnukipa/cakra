<section id="produk">
    <div class="container">
        <h3 style="margin-bottom: 3rem">Produk Cakra</h3>
        <div class="row">
            <div class="col s12 m12 l4">
                <div class="edisi-content" style="background-color: rgba(205, 127, 52, .1)">
                        <div class="detail-manfaat" >
                            <h2 style="color: rgba(205, 127, 52, 1)">Cakra Bronze</h2>
                            <p style="color: rgba(205, 127, 52, .8); text-align: center">Free Version</p>
                        </div>
                        <table class="striped bronze">
                            <tbody>
                                <tr style="background-color: rgba(205, 127, 52, 1); color: white;border: none">
                                    <td style="border: none; padding: 5px" colspan="2">TAHAPAN TERAPI</td>
                                </tr>
                                <tr style="border-top: none">
                                    <td>Tahap Dasar</td>
                                    <td>10 jenis</td>
                                </tr>
                                <tr>
                                    <td>Tahap Menengah</td>
                                    <td style="padding: 0"><i class="material-icons">close</i></td>
                                </tr>
                                <tr style="border-bottom: none">
                                    <td>Tahap Lanjut</td>
                                    <td style="padding: 0"><i class="material-icons">close</i></td>
                                </tr>

                                <tr style="background-color: rgba(205, 127, 52, 1); color: white;border: none">
                                    <td style="border: none; padding: 5px" colspan="2">FASILITAS</td>
                                </tr>
                                <tr style="border-top: none">
                                    <td>Tahap Terintruksi</td>
                                    <td style="padding: 0"><i class="material-icons">close</i></td>
                                </tr>
                                <tr>
                                    <td>Mentoring Bulanan</td>
                                    <td style="padding: 0"><i class="material-icons">check</i></td>
                                </tr>
                                <tr>
                                    <td>Laporan Perkembangan</td>
                                    <td>4 jenis</td>
                                </tr>
                                <tr style="border-bottom: none">
                                    <td>Pemakaian</td>
                                    <td>Selamanya</td>
                                </tr>

                                <tr style="background-color: rgba(205, 127, 52, 1); color: white;border: none">
                                    <td style="border: none; padding: 5px" colspan="2">KONTEN</td>
                                </tr>
                                <tr style="border-top: none">
                                    <td>Resources</td>
                                    <td>26</td>
                                </tr>
                                <tr>
                                    <td>Sound/Video</td>
                                    <td>Tetap</td>
                                </tr>
                                <tr>
                                    <td>Informasi</td>
                                    <td style="padding: 0"><i class="material-icons">check</i></td>
                                </tr>
                                <tr>
                                    <td>Video Tutorial</td>
                                    <td style="padding: 0"><i class="material-icons">close</i></td>
                                </tr>
                            </tbody>
                        </table>
                        <p style="text-align: center; margin: 25px 0 0 0">
                            <a style=" width: 100%;" href="<?php echo site_url('home/cakra') ?>" class="btn-flat bayangan_2dp" data-warna="deep-blue">
                                Download Gratis
                            </a> 
                        </p>
                    </div>
            </div>
            <div class="col s12 m12 l4">
                <div class="edisi-content" style="background-color: rgba(166, 166, 166, .1)">
                        <div class="detail-manfaat" >
                            <h2 style="color: rgba(166, 166, 166, 1)">Cakra Silver</h2>
                            <p style="text-align: center; color: rgba(166, 166, 166, .8)">Premium Version</p>
                        </div>
                        <table class="striped silver">
                            <tbody>
                                <tr style="background-color: rgba(166, 166, 166, 1); color: white;border: none">
                                    <td style="border: none; padding: 5px" colspan="2">TAHAPAN TERAPI</td>
                                </tr>
                                <tr style="border-top: none">
                                    <td>Tahap Dasar</td>
                                    <td>38 jenis</td>
                                </tr>
                                <tr>
                                    <td>Tahap Menengah</td>
                                    <td style="padding: 0"><i class="material-icons">close</i></td>
                                </tr>
                                <tr style="border-bottom: none">
                                    <td>Tahap Lanjut</td>
                                    <td style="padding: 0"><i class="material-icons">close</i></td>
                                </tr>

                                <tr style="background-color: rgba(166, 166, 166, 1); color: white;border: none">
                                    <td style="border: none; padding: 5px" colspan="2">FASILITAS</td>
                                </tr>
                                <tr style="border-top: none">
                                    <td>Tahap Terintruksi</td>
                                    <td>1 tahap</td>
                                </tr>
                                <tr>
                                    <td>Mentoring Bulanan</td>
                                    <td style="padding: 0"><i class="material-icons">check</i></td>
                                </tr>
                                <tr>
                                    <td>Laporan Perkembangan</td>
                                    <td>4 jenis</td>
                                </tr>
                                <tr style="border-bottom: none">
                                    <td>Pemakaian</td>
                                    <td>Selamanya</td>
                                </tr>

                                <tr style="background-color: rgba(166, 166, 166, 1); color: white;border: none">
                                    <td style="border: none; padding: 5px" colspan="2">KONTEN</td>
                                </tr>
                                <tr style="border-top: none">
                                    <td>Resources</td>
                                    <td>480+</td>
                                </tr>
                                <tr>
                                    <td>Sound/Video</td>
                                    <td>Berubah</td>
                                </tr>
                                <tr>
                                    <td>Informasi</td>
                                    <td style="padding: 0"><i class="material-icons">check</i></td>
                                </tr>
                                <tr>
                                    <td>Video Tutorial</td>
                                    <td style="padding: 0"><i class="material-icons">check</i></td>
                                </tr>
                            </tbody>
                        </table>
                        <p style="text-align: center; margin: 25px 0 0 0">
                            <a style=" width: 100%;" href="<?php echo site_url('home/cakra') ?>" class="btn-flat bayangan_2dp" data-warna="cakra">Pesan</a> 
                        </p>
                    </div>
            </div>
            <div class="col s12 m12 l4">
                <div class="edisi-content" style="background-color: rgba(211, 174, 55, .1)">
                        <div class="detail-manfaat" >
                            <h2 style="color: rgba(211, 174, 55, 1)">Cakra Gold</h2>
                            <p style="text-align: center; color: rgba(211, 174, 55, .8)">Premium Version</p>
                        </div>
                        <table class="striped gold">
                            <tbody>
                                <tr style="background-color: rgba(211, 174, 55, 1); color: white;border: none">
                                    <td style="border: none; padding: 5px" colspan="2">TAHAPAN TERAPI</td>
                                </tr>
                                <tr style="border-top: none">
                                    <td>Tahap Dasar</td>
                                    <td>38 jenis</td>
                                </tr>
                                <tr>
                                    <td>Tahap Menengah</td>
                                    <td>56 jenis</td>
                                </tr>
                                <tr style="border-bottom: none">
                                    <td>Tahap Lanjut</td>
                                    <td>28 jenis</td>
                                </tr>

                                <tr style="background-color: rgba(211, 174, 55, 1); color: white;border: none">
                                    <td style="border: none; padding: 5px" colspan="2">FASILITAS</td>
                                </tr>
                                <tr style="border-top: none">
                                    <td>Tahap Terintruksi</td>
                                    <td>3 tahap</td>
                                </tr>
                                <tr>
                                    <td>Mentoring Bulanan</td>
                                    <td style="padding: 0"><i class="material-icons">check</i></td>
                                </tr>
                                <tr>
                                    <td>Laporan Perkembangan</td>
                                    <td>4 jenis</td>
                                </tr>
                                <tr style="border-bottom: none">
                                    <td>Pemakaian</td>
                                    <td>Selamanya</td>
                                </tr>

                                <tr style="background-color: rgba(211, 174, 55, 1); color: white;border: none">
                                    <td style="border: none; padding: 5px" colspan="2">KONTEN</td>
                                </tr>
                                <tr style="border-top: none">
                                    <td>Resources</td>
                                    <td>1670+</td>
                                </tr>
                                <tr>
                                    <td>Sound/Video</td>
                                    <td>Berubah</td>
                                </tr>
                                <tr>
                                    <td>Informasi</td>
                                    <td style="padding: 0"><i class="material-icons">check</i></td>
                                </tr>
                                <tr>
                                    <td>Video Tutorial</td>
                                    <td style="padding: 0"><i class="material-icons">check</i></td>
                                </tr>
                            </tbody>
                        </table>
                        <p style="text-align: center; margin: 25px 0 0 0">
                            <a style=" width: 100%;" href="<?php echo site_url('home/cakra') ?>" class="btn-flat bayangan_2dp" data-warna="cakra">Pesan</a> 
                        </p>
                    </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="cerita-opening animated slideInRight">
            <a><i class="material-icons left">card_giftcard</i>Apa saja yang akan Anda dapatkan dengan membeli Produk Cakra?</a>
            <br>
            <p>
                <a style="color: #084037; font-size: 1.2rem;">
                    <i class="material-icons left" style="font-size: 1.5rem">check</i>
                    Keanggotaan CAKRA seumur hidup, tidak ada biaya bulanan.
                    <br>
                </a>
                <a style="color: #084037; font-size: 1.2rem;">
                    <i class="material-icons left" style="font-size: 1.5rem">check</i>
                    Aplikasi CAKRA yang dikemas dalam DVD yang langsung dikirim ke rumah Anda.
                    <br>
                </a>
                <a style="color: #084037; font-size: 1.2rem;">
                    <i class="material-icons left" style="font-size: 1.5rem">check</i>
                    FREE 3 bulan Pendampingan melalui : Official LINE, Official WhatsApp Group, Facebook, Twitter, SMS, Telepon. 
                </a>
                <a style="color: #084037; font-size: 1.2rem;">
                    <i class="material-icons left" style="font-size: 1.5rem">check</i>
                    100% Money-back GUARANTEE
                    <br>
                </a>
            </p>
        </div>
    </div>
</section>