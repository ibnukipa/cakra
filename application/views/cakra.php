<section id="cakra">
	<div class="container">
		<div class="gambar" style="padding: 30px 0 0 0">
            <img class="logo-cakra" src="<?php echo base_url(); ?>assets/img/logo.svg">
        </div>
        <div class="cerita-opening">
            <p>
                <strong>Cakra</strong> adalah aplikasi terapi autisme yang praktis, mudah, dan menyenangkan. 
                CAKRA dapat dijalankan di PC berbasis Windows, Linux, dan Mac. 
                Saat ini juga sedang dikembangkan versi mobile untuk Android dan iOS.
            </p>

            <p>
            	Cakra memiliki <strong>puluhan modul terapi</strong> yang bisa disesuaikan penggunaannya sesuai kondisi yang menerima terapi. 
            </p>

        </div>
    </div>

    <div class="pernyataan" style="margin-bottom: 0">
        <h5>Kenapa menggunakan Cakra?</h5>
        <div class="container">
	        <div class="row">
	        	<div class="col s12 m12 l4">
	        		<div class="lingkaran-bg">
		        		<div class="gambar">
				            <img src="<?php echo base_url(); ?>assets/img/intensif.png">
				        </div>
				    </div>
				    <div class="detail-manfaat">
					    <h4>INTENSIF</h4>
					    <p style="color: rgba(255,255,255, .8);">Aplikasi cakra membantu anda mengintensifkan terapi di rumah, sehingga dapat meningkatkan perkembangan kemampuan anak</p>
					</div>
	        	</div>
	        	<div class="col s12 m12 l4">
	        		<div class="lingkaran-bg">
		        		<div class="gambar">
				            <img src="<?php echo base_url(); ?>assets/img/dekat.png">
				        </div>
				    </div>
				    <div class="detail-manfaat">
					    <h4>DEKAT</h4>
					    <p style="color: rgba(255,255,255, .8);">Aplikasi cakra dapat menciptakan rumah sebagai tempat terapi terdekat, sehingga dapat melatih adaptasi dan sosialisasi anak</p>
					</div>
	        	</div>
	        	<div class="col s12 m12 l4">
	        		<div class="lingkaran-bg">
		        		<div class="gambar">
				            <img src="<?php echo base_url(); ?>assets/img/tekno.png">
				        </div>
				    </div>
				    <div class="detail-manfaat">
					    <h4>TEKNOLOGI</h4>
					    <p style="color: rgba(255,255,255, .8);">Menggunakan teknologi sebagai stimulus untuk mempercepat perkembangan motorik, bicara, dan akademik anak berkebutuhan khusus</p>
					</div>
	        	</div>
	        </div>
	    </div>
    </div>
    <section id="produk">
	    <div class="container">
	        <div style="background-color: white; margin-top: 80px">
	        	<!-- <h3>Produk Cakra</h3> -->
	            <div class="row">
		        	
		        	<div class="col s12 m4 l4">
		        		<div style="width: 100%; padding: 0; margin-bottom: 5px">
		        			<img style="padding: 0 15px; width: 100%;" src="<?php echo site_url('assets/img/cb.svg')?>">
		        		</div>
		        		<div class="edisi-content" style="background-color: rgba(205, 127, 52, .1)">
			        		<!-- <div class="lingkaran-bg" style="border: 1px solid rgba(205, 127, 52, 1)">
				        		<div class="gambar">
						            <i class="material-icons" style="color: rgba(205, 127, 52, 1)">star_border</i>
						        </div>
						    </div> -->
						    <!-- <div class="detail-manfaat" >
							    <h2 style="color: rgba(205, 127, 52, 1)">Cakra Bronze</h2>
							    <p style="color: rgba(205, 127, 52, .8)">Free Version</p>
							</div> -->
							<table class="striped bronze">
	                            <tbody>
	                                <tr style="background-color: rgba(205, 127, 52, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">TAHAPAN TERAPI</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Tahap Dasar</td>
	                                    <td>10 jenis</td>
	                                </tr>
	                                <tr>
	                                    <td>Tahap Menengah</td>
	                                    <td style="padding: 0"><i class="material-icons">close</i></td>
	                                </tr>
	                                <tr style="border-bottom: none">
	                                    <td>Tahap Lanjut</td>
	                                    <td style="padding: 0"><i class="material-icons">close</i></td>
	                                </tr>

	                                <tr style="background-color: rgba(205, 127, 52, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">FASILITAS</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Tahap Terintruksi</td>
	                                    <td style="padding: 0"><i class="material-icons">close</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Mentoring Bulanan</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Laporan Perkembangan</td>
	                                    <td>4 jenis</td>
	                                </tr>
	                                <tr style="border-bottom: none">
	                                    <td>Pemakaian</td>
	                                    <td>Selamanya</td>
	                                </tr>

	                                <tr style="background-color: rgba(205, 127, 52, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">KONTEN</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Resources</td>
	                                    <td>26</td>
	                                </tr>
	                                <tr>
	                                    <td>Sound/Video</td>
	                                    <td>Tetap</td>
	                                </tr>
	                                <tr>
	                                    <td>Informasi</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Video Tutorial</td>
	                                    <td style="padding: 0"><i class="material-icons">close</i></td>
	                                </tr>
	                            </tbody>
	                        </table>
							<p style="text-align: center; margin-top: 15px;">
								<?php if(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) { ?>
				                <a target="_blank" style=" width: 100%;" href="<?php  echo site_url('dashboard/download'); ?>" class="btn-flat bayangan_2dp" data-warna="deep-blue">Download Gratis</a> 
				                <?php } else { ?>
				                	<a style=" width: 100%;" href="<?php  echo site_url('user/download'); ?>" class="btn-flat bayangan_2dp" data-warna="deep-blue">Download Gratis</a> 
				                <?php } ?>
				            </p>
						</div>
						
		        	</div>

		        	<div class="col s12 m4 l4">
		        		<div style="width: 100%; padding: 0; margin-bottom: 5px">
		        			<img style="padding: 0 15px; width: 100%;" src="<?php echo site_url('assets/img/cs.svg')?>">
		        		</div>

		        		<div class="edisi-content" style="background-color: rgba(166, 166, 166, .1)">
			        		<!-- <div class="lingkaran-bg" style="border: 1px solid rgba(166, 166, 166, 1)">
				        		<div class="gambar">
						            <i class="material-icons" style="color: rgba(166, 166, 166, 1)">star_half</i>
						        </div>
						    </div>
						    <div class="detail-manfaat" >
							    <h2 style="color: rgba(166, 166, 166, 1)">Cakra Silver</h2>
							    <p style="color: rgba(166, 166, 166, .8)">Premium Version</p>
							</div> -->

							<table class="striped silver">
	                            <tbody>
	                                <tr style="background-color: rgba(166, 166, 166, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">TAHAPAN TERAPI</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Tahap Dasar</td>
	                                    <td>38 jenis</td>
	                                </tr>
	                                <tr>
	                                    <td>Tahap Menengah</td>
	                                    <td style="padding: 0"><i class="material-icons">close</i></td>
	                                </tr>
	                                <tr style="border-bottom: none">
	                                    <td>Tahap Lanjut</td>
	                                    <td style="padding: 0"><i class="material-icons">close</i></td>
	                                </tr>

	                                <tr style="background-color: rgba(166, 166, 166, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">FASILITAS</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Tahap Terintruksi</td>
	                                    <td>1 tahap</td>
	                                </tr>
	                                <tr>
	                                    <td>Mentoring Bulanan</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Laporan Perkembangan</td>
	                                    <td>4 jenis</td>
	                                </tr>
	                                <tr style="border-bottom: none">
	                                    <td>Pemakaian</td>
	                                    <td>Selamanya</td>
	                                </tr>

	                                <tr style="background-color: rgba(166, 166, 166, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">KONTEN</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Resources</td>
	                                    <td>480+</td>
	                                </tr>
	                                <tr>
	                                    <td>Sound/Video</td>
	                                    <td>Berubah</td>
	                                </tr>
	                                <tr>
	                                    <td>Informasi</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Video Tutorial</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                            </tbody>
	                        </table>
							<p style="text-align: center; margin-top: 15px;">
				                <a style=" width: 100%;" href="<?php echo site_url('home/cakra') ?>" class="btn-flat bayangan_2dp" data-warna="cakra">
				                	Beli Sekarang
				                </a> 
				            </p>
						</div>
						
		        	</div>
		        	
		        	<div class="col s12 m4 l4">
		        		<div style="width: 100%; padding: 0; margin-bottom: 5px">
		        			<img style="padding: 0 15px; width: 100%;" src="<?php echo site_url('assets/img/cg.svg')?>">
		        		</div>
		        		<div class="edisi-content" style="background-color: rgba(211, 174, 55, .1)">
			        		<!-- <div class="lingkaran-bg" style="border: 1px solid rgba(211, 174, 55, 1)">
				        		<div class="gambar">
						            <i class="material-icons" style="color: rgba(211, 174, 55, 1)">star</i>
						        </div>
						    </div>
						    <div class="detail-manfaat" >
							    <h2 style="color: rgba(211, 174, 55, 1)">Cakra Gold</h2>
							    <p style="color: rgba(211, 174, 55, .8)">Premium Version</p>
							</div> -->
							<table class="striped gold">
	                            <tbody>
	                                <tr style="background-color: rgba(211, 174, 55, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">TAHAPAN TERAPI</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Tahap Dasar</td>
	                                    <td>38 jenis</td>
	                                </tr>
	                                <tr>
	                                    <td>Tahap Menengah</td>
	                                    <td>56 jenis</td>
	                                </tr>
	                                <tr style="border-bottom: none">
	                                    <td>Tahap Lanjut</td>
	                                    <td>28 jenis</td>
	                                </tr>

	                                <tr style="background-color: rgba(211, 174, 55, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">FASILITAS</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Tahap Terintruksi</td>
	                                    <td>3 tahap</td>
	                                </tr>
	                                <tr>
	                                    <td>Mentoring Bulanan</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Laporan Perkembangan</td>
	                                    <td>4 jenis</td>
	                                </tr>
	                                <tr style="border-bottom: none">
	                                    <td>Pemakaian</td>
	                                    <td>Selamanya</td>
	                                </tr>

	                                <tr style="background-color: rgba(211, 174, 55, 1); color: white;border: none">
	                                    <td style="border: none; padding: 5px" colspan="2">KONTEN</td>
	                                </tr>
	                                <tr style="border-top: none">
	                                    <td>Resources</td>
	                                    <td>1670+</td>
	                                </tr>
	                                <tr>
	                                    <td>Sound/Video</td>
	                                    <td>Berubah</td>
	                                </tr>
	                                <tr>
	                                    <td>Informasi</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                                <tr>
	                                    <td>Video Tutorial</td>
	                                    <td style="padding: 0"><i class="material-icons">check</i></td>
	                                </tr>
	                            </tbody>
	                        </table>
							<p style="text-align: center; margin-top: 15px;">
				                <a style="width: 100%;" href="<?php echo site_url('home/cakra') ?>" class="btn-flat bayangan_2dp" data-warna="cakra">
				                	Beli Sekarang
				                </a> 
				            </p>
						</div>
						
		        	</div>
		        	
		        </div>
			<!-- <p style="text-align: center; margin: 0">
		        <a style="width: 200px;" href="<?php echo site_url('home/produk') ?>" class="btn-flat bayangan_2dp" data-warna="grey">detail produk...</a> 
		    </p> -->
	        </div>
	    </div>
	</section>

    <div class="container">
        <div class="cerita-opening" style="margin-top: 1rem;">
            <p>Selain beragam modul terapi yang ditawarkan, CAKRA juga menyediakan fitur monitoring perkembangan kondisi anak yang diterapi. Laporan Perkembangan kondisi anak disajikan dalam bentuk grafik diagram. </p>
            <p>Bagaimana tanggapan masyarakat terkait pengembangan CAKRA?</p>
            <div class="slider" style="margin-top: 15px;">
				<ul class="slides" style="background-color: #1ACDB0">
					<li>
						<div class="caption left-align">
							<div class="row" style="height: 100%">
								<div class="col s12 m4 l4 hide-on-727" style="height: 100%">
									<div class="gambar">
						                <img src="<?php echo base_url(); ?>assets/img/testimoni/illy.jpg">
						            </div>
								</div>
								<div class="col s12 m8 l8 max-on-727">
									<h5>hj. illy yudiono</h5>
									<p><i>Pakar autis dan pemilik Cakra Autism Center, Surabaya</i></p>
									<p>"Cakra adalah aplikasi yang lengkap dan sesuai untuk penyandang autis."</p>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="caption left-align">
							<div class="row" style="height: 100%">
								<div class="col s12 m4 l4 hide-on-727" style="height: 100%">
									<div class="gambar">
						                <img src="<?php echo base_url(); ?>assets/img/testimoni/fitri.jpg">
						            </div>
								</div>
								<div class="col s12 m8 l8 max-on-727">
									<h5>fitriani</h5>
									<p><i>Guru di tempat terapi</i></p>
									<p>"Aplikasi yang sangat bagus. kita belum pernah menggunakan aplikasi seperti ini."</p>
								</div>
							</div>
						</div>
					</li>

					<li>
						<div class="caption left-align">
							<div class="row" style="height: 100%">
								<div class="col s12 m4 l4 hide-on-727" style="height: 100%">
									<div class="gambar">
						                <img src="<?php echo base_url(); ?>assets/img/testimoni/yudi.jpg">
						            </div>
								</div>
								<div class="col s12 m8 l8 max-on-727">
									<h5>h. yudiono</h5>
									<p><i>pemilik yayasan autis</i></p>
									<p>"Cakra dapat bersaing dengan aplikasi lain yang sudah beredar lama di pasaran."</p>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="caption left-align">
							<div class="row" style="height: 100%">
								<div class="col s12 m4 l4 hide-on-727" style="height: 100%">
									<div class="gambar">
						                <img src="<?php echo base_url(); ?>assets/img/testimoni/fatir.jpg">
						            </div>
								</div>
								<div class="col s12 m8 l8 max-on-727">
									<h5>Fatir</h5>
									<p><i>Anak penyandang autis</i></p>
									<p>"Bagus."</p>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="caption left-align">
							<div class="row" style="height: 100%">
								<div class="col s12 m4 l4 hide-on-727" style="height: 100%">
									<div class="gambar">
						                <img src="<?php echo base_url(); ?>assets/img/testimoni/lolly.jpg">
						            </div>
								</div>
								<div class="col s12 m8 l8 max-on-727">
									<h5>ir. lolly amalia abdullah, m.sc.</h5>
									<p><i>Kepala Pusat LITBANG Aplikasi Informatika dan IKP Kementrian Pariwisata dan Ekonomi Kreatif</i></p>
									<p>"Cakra adalah aplikasi yang sangat berpotensi."</p>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="caption left-align">
							<div class="row" style="height: 100%">
								<div class="col s12 m4 l4 hide-on-727" style="height: 100%">
									<div class="gambar">
						                <img src="<?php echo base_url(); ?>assets/img/testimoni/andre.jpg">
						            </div>
								</div>
								<div class="col s12 m8 l8 max-on-727">
									<h5>andreas diantoro</h5>
									<p><i>President Director of Microsoft Indonesia</i></p>
									<p>"Cakra, aplikasi terapi autis terlengkap yang saya temui, dan sangat bermanfaat."</p>
								</div>
							</div>
						</div>
					</li>
					
				</ul>
				<div class="indi-cakra">
					<i onclick="slide_prev()" class="material-icons">keyboard_arrow_left</i>
					<i onclick="slide_next()" class="material-icons right">keyboard_arrow_right</i>
				</div>
			</div>
        </div>
    </div>

    <div class="container">
        <div class="cerita-opening animated slideInRight">
            <a><i class="material-icons left">card_giftcard</i>Apa saja yang akan Anda dapatkan dengan membeli Produk Cakra?</a>
            <br>
            <p>
                <a style="color: #084037; font-size: 1.2rem;">
                    <i class="material-icons left" style="font-size: 1.5rem">check</i>
                    Keanggotaan CAKRA seumur hidup, tidak ada biaya bulanan.
                    <br>
                </a>
                <a style="color: #084037; font-size: 1.2rem;">
                    <i class="material-icons left" style="font-size: 1.5rem">check</i>
                    Aplikasi CAKRA yang dikemas dalam DVD yang langsung dikirim ke rumah Anda.
                    <br>
                </a>
                <a style="color: #084037; font-size: 1.2rem;">
                    <i class="material-icons left" style="font-size: 1.5rem">check</i>
                    FREE 3 bulan Pendampingan melalui : Official LINE, Official WhatsApp Group, Facebook, Twitter, SMS, Telepon. 
                </a>
                <a style="color: #084037; font-size: 1.2rem;">
                    <i class="material-icons left" style="font-size: 1.5rem">check</i>
                    100% Money-back GUARANTEE
                    <br>
                </a>
            </p>
        </div>
    </div>
    

</section>