<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class Order_model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	/**
	 * create_user function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function create_pesanan($username, $data) {
		
		$data = array(
			'webuser_id'   		=> $username,
			'created' 			=> date('Y-m-j H:i:s'),
			'status'      		=> 'pending',
			'edition'   		=> $data['versi'],
			'address'			=> $data['alamat'],	
		);
		// var_dump($data);
		return $this->db->insert('order', $data);
	}

	public function get_all_pesanan_user($id_user) {

		$this->db->where('webuser_id', $id_user);
		return $this->db->get('order')->result();
	}

	public function get_pesanan($id) {
		$this->db->from('order');
		$this->db->where('id', $id);
		return $this->db->get()->row();
	}

	public function delete_order($id) {
		return $this->db->delete('order', array('id' => $id)); 
	}

	public function record_count($username) {
		$query = $this->db->where('webuser_id', $username)->get('order');
		return $query->num_rows();
	}
	public function record_count_all() {
		$query = $this->db->get('order');
		return $query->num_rows();
	}

	public function fetch_order($limit, $start, $username) {
		$this->db->limit($limit, $start);

		$query = $this->db->where('webuser_id', $username)->get('order');

		if ($query->num_rows() > 0 ) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}

			return $data;
		}

		return false;
	}

	public function fetch_order_all($limit, $start) {
		$this->db->limit($limit, $start);
		$query = $this->db->get('order');

		if ($query->num_rows() > 0 ) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}

			return $data;
		}

		return false;
	}
	
}
