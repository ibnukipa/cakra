<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class Admin_model extends CI_Model {

	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	public function create_user($username, $email, $password, $alamat, $telp, $profesi) {
		
		$data = array(
			'name'   		=> $username,
			'email'      	=> $email,
			'password'   	=> password_hash($password, PASSWORD_DEFAULT),
			'address'		=> $alamat,
			'phone'			=> $telp,
			'profession'	=> $profesi,
			'type'			=> 'user',
			// 'created_at' => date('Y-m-j H:i:s'),
		);

		$this->db->where('email', $email);
		$query = $this->db->get('webuser');
		
		if($query->num_rows() > 0)
			return false;
		else
			return $this->db->insert('webuser', $data);
		
	}

	public function update_user($email, $data){

		$this->db->where('email', $email);

		return $this->db->update('webuser', $data);
		
	}
	
	public function resolve_user_login($username, $password) {
		
		$this->db->select('password');
		$this->db->from('webuser');
		$this->db->where('email', $username);
		$hash = $this->db->get()->row('password');

		return password_verify($password, $hash);
	}
	
	public function get_user($email) {
		
		$this->db->from('webuser');
		$this->db->where('email', $email);
		return $this->db->get()->row();
		
	}

	public function verify_download($email, $kode) {

		$this->db->where('email', $email);
		$query = $this->db->get('webuser')->row();

		$true_kode = MD5($email . 'cakra01');

		if($kode == $true_kode)
			return true;
		else
			return false;
	}
}
