<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
        parent::__construct();
        if(!$this->is_login()){
            redirect("user");
        }

        $this->load->model('user_model');
        $this->load->model('order_model');
    }

    // dashboard
	public function index()
	{
		// $data['page_title'] = 'Home';
		$data['menu_aktif'] = 'dash';
		$data['no_footer'] = true;

		$data['nama_page'] = "Dashboard";
		$data['path_']          = array( 0 => 'Home' );


		$data['user_profile'] = $this->user_model->get_user($_SESSION['username']);
		$data['riwayat']	  = $this->order_model->get_all_pesanan_user($data['user_profile']->id);

		$this->load->view('template/header', $data);
		$this->load->view('dashboard/menu', $data);
		$this->load->view('dashboard/dash', $data);
		$this->load->view('dashboard/footer', $data);
	}

	public function download() {
		redirect('dashboard/download_file', 'refresh');
	}

	public function download_file() {
		$this->load->helper('download');
		if($this->user_model->verify_download($_SESSION['username'], $_SESSION['kode'])) {
			$data = file_get_contents(base_url()."assets/cakra_bronze_v1.zip");
			$name = 'CAKRA_Bronze_Setup.zip';

			force_download($name, $data);
			redirect('dashboard');
		}
	}

	//riwayat
	public function riwayat() {
		// $data['page_title'] = 'Home';
		$data['menu_aktif'] = 'riwayat';
		$data['no_footer'] = true;

		$data['nama_page'] = "Dashboard";
		$data['path_']          = array( 0 => 'Riwayat Pemesanan' );

		$data['user_profile'] = $this->user_model->get_user($_SESSION['username']);
		$data['riwayat']	  = $this->order_model->get_all_pesanan_user($data['user_profile']->id);

		$this->load->view('template/header', $data);
		$this->load->view('dashboard/menu', $data);
		$this->load->view('dashboard/riwayat', $data);
		$this->load->view('dashboard/footer', $data);	
	}

	public function cara_order() {
		$data['menu_aktif'] = 'cara_order';
		$data['no_footer'] = true;

		$data['nama_page'] = "Dashboard";
		$data['path_']          = array( 0 => 'Cara Order' );

		$this->load->view('template/header', $data);
		$this->load->view('dashboard/menu', $data);
		$this->load->view('dashboard/cara_order', $data);
		$this->load->view('dashboard/footer', $data);	
	}

	public function logout() {
		if($this->is_login()) {
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }
        }
        redirect('user');
	}

}