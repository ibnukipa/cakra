<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
        parent::__construct();
        if($this->is_login()) {
        	redirect('dashboard');
        }
        $this->load->model('user_model');
    }

    // home
	public function index()
	{

		$data['no_footer'] = true;

		// $this->session->set_flashdata('pesan','Username dan atau password salah');
		// $this->session->set_flashdata('tipe', 'error');
		
		$this->load->view('template/header', $data);
		$this->load->view('user/login', $data);
		$this->load->view('template/footer', $data);
	}

	public function login($url = '') {
		$email			= $this->input->post('email');
    	$password 	    = $this->input->post('password');

    	if($this->user_model->resolve_user_login($email, $password)) {
    		$this->set_session_user($email, $password);
    		if($url== 'download') {
	    		redirect('dashboard/download');
	    	}
    		redirect('dashboard');
    	} else {
    		$this->session->set_flashdata('pesan','<strong>Gagal!</strong> email dan atau password salah');
			$this->session->set_flashdata('tipe', 'error');
    		redirect('user');
    	}
	}

	public function daftar() {
		$data['no_footer'] = true;
		$this->load->view('template/header', $data);
		$this->load->view('user/daftar', $data);
		$this->load->view('template/footer', $data);
	}

	public function submit_user(){
		$username 	= $this->input->post('nama');
		$email 		= $this->input->post('email');
		$password 	= $this->input->post('password');
		$alamat 	= $this->input->post('alamat');
		$telp 		= $this->input->post('nohp');
		$profesi 	= $this->input->post('profesi');
		
		if($this->user_model->create_user($username, $email, $password, $alamat, $telp, $profesi)) {
			$this->session->set_flashdata('pesan','<strong>Selamat</strong> akun Anda berhasil diaktifkan!');
			$this->session->set_flashdata('tipe', 'success');
			redirect('user');
		} else {
			$this->session->set_flashdata('pesan','<strong>Gagal!</strong> email Anda sudah terdaftar');
			$this->session->set_flashdata('tipe', 'error');
			redirect('user/daftar');
		}
	}

	public function download() {
		$this->session->set_flashdata('pesan','<strong>Maaf!</strong> Anda harus login terlebih dahulu');
		$this->session->set_flashdata('tipe', 'error');
		redirect('user');
	}

	public function lupa_password() {

	}

	private function set_session_user($email, $password) {
        $_SESSION['logged_in']      = (bool)true;
        $_SESSION['username']       = $email;
        $_SESSION['kode']			= MD5($email . 'cakra01');
    }

}