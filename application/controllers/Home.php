<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
        parent::__construct();
    }

    // home
	public function index()
	{
		$data['page_title'] = 'Home';
		$data['menu_aktif'] = 'home';
		$this->load->view('template/header', $data);
		$this->load->view('template/menu', $data);
		$this->load->view('home', $data);
		$this->load->view('template/footer', $data);
	}

	public function autisme() {
		$data['page_title'] = 'Apa itu Autisme?';
		$data['menu_aktif'] = 'ttgcakra';
		$this->load->view('template/header', $data);
		$this->load->view('template/menu', $data);
		$this->load->view('autisme', $data);
		$this->load->view('template/footer', $data);	
	}

	public function cakra() {
		$data['page_title'] = 'Cakra';
		$data['menu_aktif'] = 'ttgcakra';
		$this->load->view('template/header', $data);
		$this->load->view('template/menu', $data);
		$this->load->view('cakra', $data);
		$this->load->view('template/footer', $data);
	}

	public function penghargaan() {
		$data['page_title'] = 'Penghargaan Cakra';
		$data['menu_aktif'] = 'ttgcakra';
		$this->load->view('template/header', $data);
		$this->load->view('template/menu', $data);
		$this->load->view('penghargaan', $data);
		$this->load->view('template/footer', $data);
	}

	public function cara_order() {
		$data['page_title'] = 'Cara Order Cakra';
		$data['menu_aktif'] = 'ttgcakra';
		$this->load->view('template/header', $data);
		$this->load->view('template/menu', $data);
		$this->load->view('cara_order', $data);
		$this->load->view('template/footer', $data);
	}

	public function download() {
		$email = $_GET['email'];
		$kode = $_GET['kode'];
		$this->load->model('user_model');
		$this->load->helper('download');
		if($this->user_model->verify_download($email, $kode)) {
			$data = file_get_contents(base_url()."assets/cakra_bronze_v1.zip");
			$name = 'CAKRA_Bronze_Setup.zip';

			force_download($name, $data);
			redirect('dashboard');
		}
	}

	public function get_bronze() {
		$this->load->model('user_model');
		$email = $_POST['email'];

		if($this->user_model->get_user($email)) {
			$this->session->set_flashdata('pesan','<strong>Gagal!</strong> email Anda sudah terdaftar');
			$this->session->set_flashdata('tipe', 'error');
			redirect('user');
		} else {
			$username 	= $_POST['nama'];
			$password 	= 'cakra' . rand(1000, 9999);
			$alamat 	= '';
			$telp 		= '';
			$profesi	= '';

			if($this->user_model->create_user($username, $email, $password, $alamat, $telp, $profesi)) {
				$this->load->library('email');
				$this->email->initialize(array(
				  'protocol' => 'smtp',
				  'smtp_host' => 'ssl://smtp.googlemail.com',
				  'smtp_port' => 465,
				  'smtp_user' => 'ibnu.dev@gmail.com',
				  'smtp_pass' => 'ibnupra1',
					'mailtype' => 'html',
					'charset' => 'iso-8859-1',
					'wordwrap' => TRUE,
				  'newline' => "\r\n"
				));

				$this->email->from('ibnu.dev@gmail.com', 'Cakra-app');

				$address = $email;

				$subject="Cakra Bronze - Free Download";	

				// $headers = "From: " . 'cakra-app.com' . "\r\n";
				// $headers .= "Reply-To: ". $email . "\r\n";
				// $headers .= "MIME-Version: 1.0\r\n";
				// $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


				$message = '<html><body>';
				$message .= '<h1>Terima Kasih, ' . $username . '</h1>';
				$message .= '<p> Silahkan download <strong>Cakra Bronze</strong> dengan mengeklik link berikut: <br>';
				$message .= 'link: ' . base_url() . 'home/download?email=' . $email . '&kode=' . MD5($email . 'cakra01') . '</p>';
				$message .= '<h5>Informasi</h5>';
				$message .= '<p>Email Anda sudah terdaftar pada sistem <a href="'.base_url().'user">Website Cakra</a> <br>';
				$message .= 'Akun Anda: <p>';
				$message .= '<p>Email 		: <strong>' .$email. '</strong></p>';
				$message .= '<p>Password 	: <strong>' .$password. '</strong></p>';
				$message .= '<p>Terima kasih telah menggunakan layanan <strong>Cakra</strong>.</p>';
				$message .= '</body></html>';

				// $message=
				// 'Terima kasih, '.$username.'!\r\n

				// Download Cakra Bronze gratis hanya dengan mengeklik link berikut:' 
				// . base_url() . 'home/download?email=' . $email . '&kode=' . MD5($email . 'cakra01') .
				// 'Email Anda telah terdaftar pada sistem Cakra-app.com. Sehingga untuk keperluan selanjutnya gunakanlah Akun berikut untuk login ke sistem cakra-app.com
				// -------------------------------------------------
				// Email   : ' . $email . '
				// Password: ' . $password . '
				// -------------------------------------------------
				        
				// Terima kasih';
				/*-----------email body ends-----------*/		      
				$this->email->to($address);
				$this->email->subject($subject);
				$this->email->message($message);
				if($this->email->send()) {
					$this->session->set_flashdata('pesan','<strong>Selamat!</strong> Silahkan cek email Anda untuk mendapatkan link download');
					$this->session->set_flashdata('tipe', 'success');
					redirect('user');
				}
			} else {
				$this->session->set_flashdata('pesan','<strong>Gagal!</strong> email Anda sudah terdaftar');
				$this->session->set_flashdata('tipe', 'error');
				redirect('user');
			}
		}
		
	}

	// public function testimonial() {
	// 	$data['page_title'] = 'Cakra';
	// 	$data['menu_aktif'] = 'ttgcakra';
	// 	$this->load->view('template/header', $data);
	// 	$this->load->view('template/menu', $data);
	// 	$this->load->view('testimonial', $data);
	// 	$this->load->view('template/footer', $data);
	// }

	// public function produk() {
	// 	$data['page_title'] = 'Cakra';
	// 	$data['menu_aktif'] = 'ttgcakra';
	// 	$this->load->view('template/header', $data);
	// 	$this->load->view('template/menu', $data);
	// 	$this->load->view('produk', $data);
	// 	$this->load->view('template/footer', $data);
	// }

}